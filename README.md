# Domain checker

Discovering rust with a small lambda

### Useful links :
https://github.com/awslabs/aws-sdk-rust  
https://docs.rs/aws-sdk-sesv2/0.21.0/aws_sdk_sesv2/index.html  
https://docs.aws.amazon.com/sdk-for-rust/latest/dg/rust_sesv2_code_examples.html  
https://docs.aws.amazon.com/ses/latest/dg/send-an-email-using-sdk-programmatically.html

### To check
- tokio

### Setup
```shell
pip3 install cargo-lambda
```
### Test
```shell
cargo lambda watch
cargo lambda invoke --data-file event.json
```

### Deploy
```shell
cargo lambda build --release --arm64 --output-format zip
cargo lambda deploy --iam-role arn:aws:iam::{{ lambda_role }}
```
