use std::env;
use std::process::exit;

use aws_lambda_events::event::cloudwatch_events::CloudWatchEvent;
use aws_sdk_sesv2::model::{Body, Content, Destination, EmailContent, Message};
use lambda_runtime::{Error, LambdaEvent, run, service_fn};
use reqwest::StatusCode;

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
/// - https://github.com/aws-samples/serverless-rust-demo/
async fn function_handler(_event: LambdaEvent<CloudWatchEvent>) -> Result<(), Error> {

    // Check url
    let url: String = match env::var("url_to_check") {
        Ok(val) => val,
        Err(_) => exit(0x01)
    };

    let status_code = reqwest::get(url)
        .await?
        .status();

    let message: &str = match status_code {
        StatusCode::OK => "Domain still there :/",
        StatusCode::NOT_FOUND => r"Domain not found, available \o/",
        _ => "Something else go check..."
    };

    let sender_email: String = match env::var("sender_email") {
        Ok(val) => val,
        Err(_) => exit(0x02)
    };

    let destination_email: String = match env::var("destination_email") {
        Ok(val) => val,
        Err(_) => exit(0x03)
    };

    // Send recap email
    let config = aws_config::load_from_env().await;
    let client = aws_sdk_sesv2::Client::new(&config);
    let dest = Destination::builder().to_addresses(destination_email).build();
    let subject_content = Content::builder().data(message).charset("UTF-8").build();
    let body_content = Content::builder().data(message).charset("UTF-8").build();
    let body = Body::builder().text(body_content).build();
    let msg = Message::builder()
        .subject(subject_content)
        .body(body)
        .build();

    let email_content = EmailContent::builder().simple(msg).build();
    client
        .send_email()
        .from_email_address(sender_email)
        .destination(dest)
        .content(email_content)
        .send()
        .await?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
